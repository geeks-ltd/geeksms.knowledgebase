using MSharp;

namespace Learn
{
    public class SearchPage : MSharp.SubPage<LearnPage>
    {
        /*M#:w[7]T-Prop:Name-Type:ApplicationPage-This page seems to be orphand. There is no standard Navigate Activity pointing to it.*/
        public SearchPage()
        {
            Name("Search").Layout(Layouts.Default);

            Add<Modules.ListSearchResults>();
        }
    }
}