using MSharp;

namespace Learn
{
    public class ContentPage : MSharp.SubPage<LearnPage>
    {
        public ContentPage()
        {
            Name("Content")
                .ValidateRequest(false)
                .Layout(Layouts.Default);

            Add<Modules.FormArticleContent>();
        }
    }
}