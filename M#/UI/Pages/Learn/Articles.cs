using MSharp;

namespace Learn
{
    public class ArticlesPage : MSharp.SubPage<LearnPage>
    {
        public ArticlesPage()
        {
            Name("Articles").Layout(Layouts.Default);

            Add<Modules.ArticleList>();
        }
    }
}