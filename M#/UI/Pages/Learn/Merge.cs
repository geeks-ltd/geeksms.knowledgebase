using MSharp;

namespace Learn
{
    public class MergePage : MSharp.SubPage<LearnPage>
    {
        public MergePage()
        {
            Name("Merge")
                .Layout(Layouts.Modal);

            Add<Modules.MergeArticleForm>();
        }
    }
}