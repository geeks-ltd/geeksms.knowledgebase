using MSharp;

namespace Learn
{
    public class EnterPage : MSharp.SubPage<LearnPage>
    {
        public EnterPage()
        {
            Name("Enter").Layout(Layouts.Default);

            Add<Modules.ListArticle>();
            Add<Modules.FormArticleRegularUser>();
        }
    }
}