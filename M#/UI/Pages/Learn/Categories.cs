using MSharp;

namespace Learn
{
    public class CategoriesPage : MSharp.SubPage<LearnPage>
    {
        public CategoriesPage()
        {
            Name("Categories").Layout(Layouts.Default);

            Add<Modules.ArticleCategoryList>();
        }
    }
}