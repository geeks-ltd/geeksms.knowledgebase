using MSharp;

namespace Settings.ArticleContents
{
    public class ReadHistoryPage : MSharp.SubPage<ArticleContentsPage>
    {
        public ReadHistoryPage()
        {
            Name("Read history")
                .Layout(Layouts.Default);

            Add<Modules.ReadHistoryList>();
        }
    }
}