using MSharp;

namespace Settings.ArticleContents
{
    public class EnterPage : MSharp.SubPage<ArticleContentsPage>
    {
        public EnterPage()
        {
            Name("Enter").Layout(Layouts.Default);

            Add<Modules.ArticleContentForm>();
        }
    }
}