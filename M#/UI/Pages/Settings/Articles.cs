using MSharp;

namespace Settings
{
    public class ArticlesPage : MSharp.SubPage<SettingsPage>
    {
        /*M#:w[7]T-Prop:Name-Type:ApplicationPage-This page seems to be orphand. There is no standard Navigate Activity pointing to it.*/
        public ArticlesPage()
        {
            Name("Articles").Layout(Layouts.Default);

            Add<Modules.ArticlesList>();
        }
    }
}