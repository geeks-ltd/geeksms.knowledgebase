using MSharp;

namespace Settings.Categories
{
    public class RenderPDFPage : MSharp.SubPage<CategoriesPage>
    {
        public RenderPDFPage()
        {
            Name("Render PDF").Layout(Layouts.Default);

            Add<Modules.CategoryArticlesRender>();
        }
    }
}