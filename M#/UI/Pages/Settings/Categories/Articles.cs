using MSharp;

namespace Settings.Categories
{
    public class ArticlesPage : MSharp.SubPage<CategoriesPage>
    {
        public ArticlesPage()
        {
            Name("Articles")
                .Layout(Layouts.Default);

            Add<Modules.ListArticleArticles>();
        }
    }
}