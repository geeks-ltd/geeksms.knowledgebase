using MSharp;

namespace Settings.Categories.Articles
{
    public class EnterPage : MSharp.SubPage<ArticlesPage>
    {
        public EnterPage()
        {
            Name("Enter").Layout(Layouts.Default);

            Add<Modules.FormArticle>();
        }
    }
}