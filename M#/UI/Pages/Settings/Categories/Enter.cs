using MSharp;

namespace Settings.Categories
{
    public class EnterPage : MSharp.SubPage<CategoriesPage>
    {
        public EnterPage()
        {
            Name("Enter")
                .Layout(Layouts.Default);

            Add<Modules.FormArticleCategory>();
        }
    }
}