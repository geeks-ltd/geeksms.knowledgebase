using MSharp;

namespace Settings
{
    public class ArticleContentsPage : MSharp.SubPage<SettingsPage>
    {
        public ArticleContentsPage()
        {
            Name("Article contents")
                .Layout(Layouts.Default);

            Add<Modules.ArticleContentsList>();
        }
    }
}