using MSharp;

namespace Settings
{
    public class CategoriesPage : MSharp.SubPage<SettingsPage>
    {
        public CategoriesPage()
        {
            Name("Categories").Layout(Layouts.Default);

            Add<Modules.ListArticleCategory>();
        }
    }
}