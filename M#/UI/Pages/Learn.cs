using MSharp;
using Olive;

public class LearnPage : MSharp.RootPage
{
    public LearnPage()
    {
        Name("Learn")
            .RootCssClass("learn")
            .Route(new string[] { "learn", "/" }.ToLinesString())
            .Layout(Layouts.Default);

        Add<Modules.ListArticle>();
        Add<Modules.ViewArticle>();
    }
}