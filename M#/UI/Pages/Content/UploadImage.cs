using MSharp;

namespace Learn.Content
{
    public class UploadImagePage : MSharp.SubPage<ContentPage>
    {
        public UploadImagePage()
        {
            Name("Upload image")
                .Layout(Layouts.Modal);

            Add<Modules.UploadImageForm>();
        }
    }
}
