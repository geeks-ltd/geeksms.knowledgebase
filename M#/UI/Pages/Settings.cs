using MSharp;

public class SettingsPage : MSharp.RootPage
{
    public SettingsPage()
    {
        Name("Settings")
            .Layout(Layouts.Default);
    }
}