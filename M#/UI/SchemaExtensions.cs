﻿// ********************************************************************
// WARNING: This file is auto-generated from M# Model.
// and may be overwritten at any time. Do not change it manually.
// ********************************************************************

namespace MSharp
{
    using System.Runtime.CompilerServices;
    
    static partial class SchemaExtensions
    {
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.Article> Subject(this ListModule<Domain.Article>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Subject, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.Article> Contents(this ListModule<Domain.Article>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Contents, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.Article> LastUpdatedDate(this ListModule<Domain.Article>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.LastUpdatedDate, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.Article> CreatedBy(this ListModule<Domain.Article>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.CreatedBy, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.Article> LastUpdatedBy(this ListModule<Domain.Article>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.LastUpdatedBy, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.Article> Category(this ListModule<Domain.Article>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Category, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.Article> ContentItems(this ListModule<Domain.Article>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.ContentItems, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.Article> Reference(this ListModule<Domain.Article>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Reference, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.Article> Order(this ListModule<Domain.Article>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Order, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleCategory> Name(this ListModule<Domain.ArticleCategory>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleCategory> Microsites(this ListModule<Domain.ArticleCategory>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Microsites, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleCategory> CreatedBy(this ListModule<Domain.ArticleCategory>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.CreatedBy, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleCategory> Articles(this ListModule<Domain.ArticleCategory>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Articles, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleCategory> AccessLevels(this ListModule<Domain.ArticleCategory>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.AccessLevels, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleCategory> Order(this ListModule<Domain.ArticleCategory>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Order, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> Subject(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Subject, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> Body(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Body, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> CollapsedInformation(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.CollapsedInformation, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> File(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.File, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> FullName(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.FullName, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> Ref(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Ref, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> LastUpdated(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.LastUpdated, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> Type(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Type, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> Article(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Article, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> UpdatedBy(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.UpdatedBy, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> Links(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Links, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> Studies(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Studies, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContent> Order(this ListModule<Domain.ArticleContent>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Order, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContentLink> Owner(this ListModule<Domain.ArticleContentLink>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Owner, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContentLink> Content(this ListModule<Domain.ArticleContentLink>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Content, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContentStudy> Updated(this ListModule<Domain.ArticleContentStudy>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Updated, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContentStudy> Content(this ListModule<Domain.ArticleContentStudy>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Content, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContentStudy> User(this ListModule<Domain.ArticleContentStudy>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.User, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleContentStudy> Article(this ListModule<Domain.ArticleContentStudy>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Article, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleScore> Name(this ListModule<Domain.ArticleScore>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ArticleScore> Score(this ListModule<Domain.ArticleScore>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Score, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.BackgroundTask> Name(this ListModule<Domain.BackgroundTask>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.BackgroundTask> ExecutingInstance(this ListModule<Domain.BackgroundTask>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.ExecutingInstance, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.BackgroundTask> Heartbeat(this ListModule<Domain.BackgroundTask>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Heartbeat, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.BackgroundTask> LastExecuted(this ListModule<Domain.BackgroundTask>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.LastExecuted, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.BackgroundTask> IntervalInMinutes(this ListModule<Domain.BackgroundTask>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.IntervalInMinutes, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.BackgroundTask> TimeoutInMinutes(this ListModule<Domain.BackgroundTask>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.TimeoutInMinutes, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ContentType> Name(this ListModule<Domain.ContentType>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ContentType> SubjectField(this ListModule<Domain.ContentType>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.SubjectField, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ContentType> BodyField(this ListModule<Domain.ContentType>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.BodyField, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ContentType> IsHtml(this ListModule<Domain.ContentType>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.IsHtml, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.ContentType> Order(this ListModule<Domain.ContentType>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Order, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.KnowledgeAccess> Category(this ListModule<Domain.KnowledgeAccess>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Category, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.KnowledgeAccess> Access(this ListModule<Domain.KnowledgeAccess>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Access, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.KnowledgeAccess> Role(this ListModule<Domain.KnowledgeAccess>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Role, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.KnowledgeAccess> Level(this ListModule<Domain.KnowledgeAccess>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Level, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<Domain.KnowledgeAccessLevel> Name(this ListModule<Domain.KnowledgeAccessLevel>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<PeopleService.Level> Name(this ListModule<PeopleService.Level>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<PeopleService.Person> Name(this ListModule<PeopleService.Person>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<PeopleService.Person> Email(this ListModule<PeopleService.Person>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Email, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<PeopleService.Person> ImageUrl(this ListModule<PeopleService.Person>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.ImageUrl, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<PeopleService.Person> IsActive(this ListModule<PeopleService.Person>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.IsActive, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<PeopleService.Person> Roles(this ListModule<PeopleService.Person>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Roles, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<PeopleService.Role> Name(this ListModule<PeopleService.Role>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static PropertyFilterElement<PeopleService.Role> FullName(this ListModule<PeopleService.Role>.SearchComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Search(x => x.FullName, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.Article> Subject(this ViewModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Subject, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.Article> Contents(this ViewModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Contents, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.Article> LastUpdatedDate(this ViewModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.LastUpdatedDate, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.Article> CreatedBy(this ViewModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.CreatedBy, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.Article> LastUpdatedBy(this ViewModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.LastUpdatedBy, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.Article> Category(this ViewModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Category, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.Article> ContentItems(this ViewModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.ContentItems, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.Article> Reference(this ViewModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Reference, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.Article> Order(this ViewModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Order, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleCategory> Name(this ViewModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleCategory> Microsites(this ViewModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Microsites, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleCategory> CreatedBy(this ViewModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.CreatedBy, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleCategory> Articles(this ViewModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Articles, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleCategory> AccessLevels(this ViewModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.AccessLevels, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleCategory> Order(this ViewModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Order, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> Subject(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Subject, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> Body(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Body, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> CollapsedInformation(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.CollapsedInformation, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> File(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.File, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> FullName(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.FullName, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> Ref(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Ref, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> LastUpdated(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.LastUpdated, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> Type(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Type, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> Article(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Article, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> UpdatedBy(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.UpdatedBy, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> Links(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Links, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> Studies(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Studies, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContent> Order(this ViewModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Order, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContentLink> Owner(this ViewModule<Domain.ArticleContentLink>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Owner, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContentLink> Content(this ViewModule<Domain.ArticleContentLink>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Content, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContentStudy> Updated(this ViewModule<Domain.ArticleContentStudy>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Updated, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContentStudy> Content(this ViewModule<Domain.ArticleContentStudy>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Content, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContentStudy> User(this ViewModule<Domain.ArticleContentStudy>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.User, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleContentStudy> Article(this ViewModule<Domain.ArticleContentStudy>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Article, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleScore> Name(this ViewModule<Domain.ArticleScore>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ArticleScore> Score(this ViewModule<Domain.ArticleScore>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Score, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.BackgroundTask> Name(this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.BackgroundTask> ExecutingInstance(this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.ExecutingInstance, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.BackgroundTask> Heartbeat(this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Heartbeat, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.BackgroundTask> LastExecuted(this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.LastExecuted, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.BackgroundTask> IntervalInMinutes(this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.IntervalInMinutes, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.BackgroundTask> TimeoutInMinutes(this ViewModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.TimeoutInMinutes, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ContentType> Name(this ViewModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ContentType> SubjectField(this ViewModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.SubjectField, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ContentType> BodyField(this ViewModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.BodyField, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ContentType> IsHtml(this ViewModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.IsHtml, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.ContentType> Order(this ViewModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Order, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.KnowledgeAccess> Category(this ViewModule<Domain.KnowledgeAccess>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Category, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.KnowledgeAccess> Access(this ViewModule<Domain.KnowledgeAccess>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Access, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.KnowledgeAccess> Role(this ViewModule<Domain.KnowledgeAccess>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Role, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.KnowledgeAccess> Level(this ViewModule<Domain.KnowledgeAccess>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Level, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<Domain.KnowledgeAccessLevel> Name(this ViewModule<Domain.KnowledgeAccessLevel>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<PeopleService.Level> Name(this ViewModule<PeopleService.Level>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<PeopleService.Person> Name(this ViewModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<PeopleService.Person> Email(this ViewModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Email, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<PeopleService.Person> ImageUrl(this ViewModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.ImageUrl, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<PeopleService.Person> IsActive(this ViewModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.IsActive, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<PeopleService.Person> Roles(this ViewModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Roles, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<PeopleService.Role> Name(this ViewModule<PeopleService.Role>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#afcd14")]
        public static ViewElement<PeopleService.Role> FullName(this ViewModule<PeopleService.Role>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.FullName, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.Article> Subject(this ListModule<Domain.Article>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Subject, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.Article> Contents(this ListModule<Domain.Article>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Contents, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.Article> LastUpdatedDate(this ListModule<Domain.Article>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.LastUpdatedDate, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.Article> CreatedBy(this ListModule<Domain.Article>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.CreatedBy, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.Article> LastUpdatedBy(this ListModule<Domain.Article>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.LastUpdatedBy, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.Article> Category(this ListModule<Domain.Article>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Category, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.Article> ContentItems(this ListModule<Domain.Article>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.ContentItems, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.Article> Reference(this ListModule<Domain.Article>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Reference, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.Article> Order(this ListModule<Domain.Article>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Order, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleCategory> Name(this ListModule<Domain.ArticleCategory>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Name, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleCategory> Microsites(this ListModule<Domain.ArticleCategory>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Microsites, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleCategory> CreatedBy(this ListModule<Domain.ArticleCategory>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.CreatedBy, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleCategory> Articles(this ListModule<Domain.ArticleCategory>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Articles, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleCategory> AccessLevels(this ListModule<Domain.ArticleCategory>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.AccessLevels, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleCategory> Order(this ListModule<Domain.ArticleCategory>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Order, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> Subject(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Subject, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> Body(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Body, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> CollapsedInformation(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.CollapsedInformation, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> File(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.File, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> FullName(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.FullName, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> Ref(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Ref, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> LastUpdated(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.LastUpdated, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> Type(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Type, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> Article(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Article, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> UpdatedBy(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.UpdatedBy, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> Links(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Links, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> Studies(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Studies, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContent> Order(this ListModule<Domain.ArticleContent>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Order, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContentLink> Owner(this ListModule<Domain.ArticleContentLink>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Owner, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContentLink> Content(this ListModule<Domain.ArticleContentLink>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Content, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContentStudy> Updated(this ListModule<Domain.ArticleContentStudy>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Updated, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContentStudy> Content(this ListModule<Domain.ArticleContentStudy>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Content, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContentStudy> User(this ListModule<Domain.ArticleContentStudy>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.User, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleContentStudy> Article(this ListModule<Domain.ArticleContentStudy>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Article, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleScore> Name(this ListModule<Domain.ArticleScore>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Name, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ArticleScore> Score(this ListModule<Domain.ArticleScore>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Score, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.BackgroundTask> Name(this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Name, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.BackgroundTask> ExecutingInstance(this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.ExecutingInstance, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.BackgroundTask> Heartbeat(this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Heartbeat, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.BackgroundTask> LastExecuted(this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.LastExecuted, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.BackgroundTask> IntervalInMinutes(this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.IntervalInMinutes, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.BackgroundTask> TimeoutInMinutes(this ListModule<Domain.BackgroundTask>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.TimeoutInMinutes, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ContentType> Name(this ListModule<Domain.ContentType>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Name, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ContentType> SubjectField(this ListModule<Domain.ContentType>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.SubjectField, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ContentType> BodyField(this ListModule<Domain.ContentType>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.BodyField, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ContentType> IsHtml(this ListModule<Domain.ContentType>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.IsHtml, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.ContentType> Order(this ListModule<Domain.ContentType>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Order, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.KnowledgeAccess> Category(this ListModule<Domain.KnowledgeAccess>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Category, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.KnowledgeAccess> Access(this ListModule<Domain.KnowledgeAccess>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Access, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.KnowledgeAccess> Role(this ListModule<Domain.KnowledgeAccess>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Role, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.KnowledgeAccess> Level(this ListModule<Domain.KnowledgeAccess>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Level, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<Domain.KnowledgeAccessLevel> Name(this ListModule<Domain.KnowledgeAccessLevel>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Name, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<PeopleService.Level> Name(this ListModule<PeopleService.Level>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Name, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<PeopleService.Person> Name(this ListModule<PeopleService.Person>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Name, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<PeopleService.Person> Email(this ListModule<PeopleService.Person>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Email, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<PeopleService.Person> ImageUrl(this ListModule<PeopleService.Person>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.ImageUrl, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<PeopleService.Person> IsActive(this ListModule<PeopleService.Person>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.IsActive, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<PeopleService.Person> Roles(this ListModule<PeopleService.Person>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Roles, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<PeopleService.Role> Name(this ListModule<PeopleService.Role>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.Name, file, line);
        
        [MethodColor("#0ccc68")]
        public static ViewElement<PeopleService.Role> FullName(this ListModule<PeopleService.Role>.ColumnComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Column(x => x.FullName, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Subject(this FormModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Subject, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Contents(this FormModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Contents, file, line);
        
        [MethodColor("#AFCD14")]
        public static DateTimeFormElement LastUpdatedDate(this FormModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.LastUpdatedDate, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement CreatedBy(this FormModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.CreatedBy, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement LastUpdatedBy(this FormModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.LastUpdatedBy, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Category(this FormModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Category, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement ContentItems(this FormModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.ContentItems, file, line);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement Reference(this FormModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Reference, file, line);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement Order(this FormModule<Domain.Article>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Order, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(this FormModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Microsites(this FormModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Microsites, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement CreatedBy(this FormModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.CreatedBy, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Articles(this FormModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Articles, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement AccessLevels(this FormModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.AccessLevels, file, line);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement Order(this FormModule<Domain.ArticleCategory>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Order, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Subject(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Subject, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Body(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Body, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement CollapsedInformation(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.CollapsedInformation, file, line);
        
        [MethodColor("#AFCD14")]
        public static BinaryFormElement File(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.File, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement FullName(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.FullName, file, line);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement Ref(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Ref, file, line);
        
        [MethodColor("#AFCD14")]
        public static DateTimeFormElement LastUpdated(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.LastUpdated, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Type(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Type, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Article(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Article, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement UpdatedBy(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.UpdatedBy, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Links(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Links, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Studies(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Studies, file, line);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement Order(this FormModule<Domain.ArticleContent>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Order, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Owner(this FormModule<Domain.ArticleContentLink>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Owner, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Content(this FormModule<Domain.ArticleContentLink>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Content, file, line);
        
        [MethodColor("#AFCD14")]
        public static DateTimeFormElement Updated(this FormModule<Domain.ArticleContentStudy>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Updated, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Content(this FormModule<Domain.ArticleContentStudy>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Content, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement User(this FormModule<Domain.ArticleContentStudy>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.User, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Article(this FormModule<Domain.ArticleContentStudy>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Article, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(this FormModule<Domain.ArticleScore>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement Score(this FormModule<Domain.ArticleScore>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Score, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(this FormModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement ExecutingInstance(this FormModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.ExecutingInstance, file, line);
        
        [MethodColor("#AFCD14")]
        public static DateTimeFormElement Heartbeat(this FormModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Heartbeat, file, line);
        
        [MethodColor("#AFCD14")]
        public static DateTimeFormElement LastExecuted(this FormModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.LastExecuted, file, line);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement IntervalInMinutes(this FormModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.IntervalInMinutes, file, line);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement TimeoutInMinutes(this FormModule<Domain.BackgroundTask>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.TimeoutInMinutes, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(this FormModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement SubjectField(this FormModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.SubjectField, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement BodyField(this FormModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.BodyField, file, line);
        
        [MethodColor("#AFCD14")]
        public static BooleanFormElement IsHtml(this FormModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.IsHtml, file, line);
        
        [MethodColor("#AFCD14")]
        public static NumberFormElement Order(this FormModule<Domain.ContentType>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Order, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Category(this FormModule<Domain.KnowledgeAccess>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Category, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Access(this FormModule<Domain.KnowledgeAccess>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Access, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Role(this FormModule<Domain.KnowledgeAccess>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Role, file, line);
        
        [MethodColor("#AFCD14")]
        public static AssociationFormElement Level(this FormModule<Domain.KnowledgeAccess>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Level, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(this FormModule<Domain.KnowledgeAccessLevel>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(this FormModule<PeopleService.Level>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(this FormModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Email(this FormModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Email, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement ImageUrl(this FormModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.ImageUrl, file, line);
        
        [MethodColor("#AFCD14")]
        public static BooleanFormElement IsActive(this FormModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.IsActive, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Roles(this FormModule<PeopleService.Person>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Roles, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement Name(this FormModule<PeopleService.Role>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.Name, file, line);
        
        [MethodColor("#AFCD14")]
        public static StringFormElement FullName(this FormModule<PeopleService.Role>.FieldComponents @this, [CallerFilePath] string file = null, [CallerLineNumber] int line = 0) => @this.module.Field(x => x.FullName, file, line);
    }
}