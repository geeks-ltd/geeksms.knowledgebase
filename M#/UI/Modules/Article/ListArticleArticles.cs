using MSharp;

namespace Modules
{
    public class ListArticleArticles : MSharp.ListModule<Domain.Article>
    {
        public ListArticleArticles()
        {
            HeaderText("@Model.Category - Articles")
                .Sortable()
                .DataSource("await info.Category.Articles.GetList()")
                .SortingStatement("item.Order");

            // ================ Columns: ================

            Column(x => x.Subject).LabelText("Article");

            Column(x => x.ContentItems).DisplayExpression(cs("await item.ContentItems.Count()"))
                .SortingStatement("await item.ContentItems.Count()")
                .SortKey("ContentItemsCount");

            LinkColumn("Preview").HeaderText("Preview").SortKey("Preview")
                .OnClick(x => x.Go(cs("item.GetUrl()")).Target(OpenIn.NewBrowserWindow));

            ButtonColumn("Move up").HeaderText("Order")
                .CssClass("btn btn-info")
                .Icon(FA.ArrowCircleUp)
            .OnClick(x =>
            {
                x.MoveUp();
                x.Reload();
            });

            ButtonColumn("Move down").HeaderText("Order")
                .CssClass("btn btn-info")
                .Icon(FA.ArrowCircleDown)
            .OnClick(x =>
            {
                x.MoveDown();
                x.Reload();
            });

            ButtonColumn("Edit").HeaderText("Edit")
                .Icon(FA.Edit)
            .OnClick(x =>
            {
                x.Go<Settings.Categories.Articles.EnterPage>().SendReturnUrl()
                    /*M#:w[44]T-Prop:Key-Type:QueryStringParameter-The destination page doesn't seem to utilise Query String 'id' anywhere.*/.Send("item", "item.ID");
            });

            ButtonColumn("DeleteCommand").HeaderText("Delete")
                .Text("Delete")
                .CssClass("btn btn-danger")
                .ConfirmQuestion("Are you sure you want to delete this Article?")
                .Icon(FA.Remove)
            .OnClick(x =>
            {
                x.DeleteItem();
                x.Reload();
            });

            ViewModelProperty<Domain.ArticleCategory>("Category").CacheValue().FromRequestParam("id");

            Button("Add article")
            .OnClick(x =>
            {
                x.Go<Settings.Categories.Articles.EnterPage>().SendReturnUrl()
                    /*M#:w[63]T-Prop:Key-Type:QueryStringParameter-The destination page doesn't seem to utilise Query String '.Category' anywhere.*/.Send("cat", "Model.Category.ID");
            });
        }
    }
}
