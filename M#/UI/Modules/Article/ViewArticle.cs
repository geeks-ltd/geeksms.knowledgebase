using Domain;
using MSharp;

namespace Modules
{
    public class ViewArticle : ViewModule<Article>
    {
        public ViewArticle()
        {
            NoEmptyMarkup()
                .RequestParam("article")
                .RootCssClass("ml-1")
                .VisibleIf("info.Item != null")
                .Markup(@"            
            <div class=""card"">
                <div class=""card-header"">@Model.Item.Subject </div>            
                <div class=""card-body"">
                    <div>
                        [#BUTTONS(AddContent)#]
                    </div>
                    <div class=""card-text""> @Model.Item.Contents.Raw() </div>
                    <div>
                        <hr/>
                        @(await Component.InvokeAsync<ListArticleContent>(new ViewModel.ListArticleContent() { Article = item }))
                    </div>
                    <div>[#BUTTONS(Edit)#][#BUTTONS(Delete)#][#BUTTONS(Merge)#] </div>
                </div>
            </div>
            ");

            // ================ Code Extensions: ================

            ViewModelProperty<Article>("Article").CacheValue().FromRequestParam("article");

            OnBound("Set browser title").Code("ViewData[\"Title\"] = \"Training\" + info.Item;");

            // ================ Buttons: ================

            Button("Edit").MarkupTemplate("<br/><hr/>[#Button#]")
                .VisibleIf("(await (await Olive.Context.Current.Person()).CanEdit(info.Item.Category))")
            .OnClick(x =>
            {
                x.If(AppRole.Director).Go<Settings.Categories.Articles.EnterPage>().SendReturnUrl()
                    .Send("item", "info.Item.ID");
                x.Go<Learn.EnterPage>().SendReturnUrl()
                    .Send("item", "info.Item.ID");
            });

            Button("Delete").Name("Delete").ConfirmQuestion("Are you sure?")
                .CssClass("btn btn-danger")
                .VisibleIf("(await (await Olive.Context.Current.Person()).CanEdit(info.Item.Category))")
            .OnClick(x =>
            {
                x.DeleteItem();
                x.Go<LearnPage>();
            });

            Button("Add content").CssClass("float-right btn btn-info")
                .VisibleIf("(await (await Olive.Context.Current.Person()).CanEdit(info.Item.Category))")
            .OnClick(x =>
            {
                x.Go<Learn.ContentPage>().SendReturnUrl()
                    .Send("article", "info.Item.ID");
            });

            Button("Merge...").CssClass("float-right").Name("Merge").VisibleIf(AppRole.Director)
            .OnClick(x =>
            {
                x.PopUp<Learn.MergePage>()
                    .Send("article", "info.Item.ID");
            });

            Reference<ListArticleContent>();
        }
    }
}
