using MSharp;

namespace Modules
{
    public class ListArticle : MSharp.ListModule<Domain.Article>
    {
        public ListArticle()
        {
            ShowHeaderRow(false)
                .Sortable()
                .Prefix("la")
                .WrapInForm(false)
                .MarkupWrapper("<div class=\"accordion\" id=\"accordionArticle\" >@{ var i = 0;} [#MODULE#]</div>")
                .DataSource("await (await Context.Current.Person()).GetMyArticles()")
                .SortingStatement("item.Order");

            /*M#:w[17]T-Prop:GroupTemplate-Type:ListGroupingExpression-There is too much custom code here. Refactor it, break it down, move logic to Model project, etc.*//*M#:w[17]T-Prop:GroupTemplate-Type:ListGroupingExpression-Do not use '<A>' directly in the code. Instead create a Button and reference it here using [#BUTTONS(ButtonName)#].*/
            Grouping(x => x.Category).GroupTemplate(@"
                <div class=""card"">
                <div class=""card-header"" id='@string.Format(""heading{0}"",i)'>
                    <h5 class=""mb-0"">
                    <button class=""btn btn-link font-weight-bold"" type=""button"" data-toggle=""collapse"" data-target='@string.Format(""#collapse{0}"",i)' aria-expanded=""false"" aria-controls='@string.Format(""collapse{0}
                        "",i)'>
                        @group.Key 
                    </button><span class=""float-right"">@group.Count() <i class=""fas fa-arrow-circle-down""></i></span>
                    </h5>
                </div>

                <div id ='@string.Format(""collapse{0}"",i)' class='@string.Format(""collapse {0}"",""show"".OnlyWhen(group.Any(s=>s.Item.ID.ToString() == Request.Param(""article""))))' aria-labelledby='@string.Format(""heading{0}"",i)' data-parent=""#accordionArticle"">
                  <div class=""card-body"">
                        @{var j = 0;}
                        @foreach (var listItem in group) {
                            if( j == 0) {
                                <div class=""add-article text-right"">
                                     [#BUTTONS(AddArticle)#]
                                </div>
                            }
                            <div class=""article"" data-a-id=""@listItem.Item.ID"" data-a-order=""@listItem.Item.Order"">
                            <a name=""Subject"" class='@(""highlighted "".OnlyWhen(Model.Article == listItem.Item))' href='@Url.Index(""Learn"", new { article = listItem.Item.ID })' data-redirect=""ajax"">@(""**********"".OnlyWhen(listItem.Item.Contents.HasValue()) + (await listItem.Item.GenerateArticleTitle()).Raw())</a>
                                <span class=""sort float-right""><i class=""fas fa-arrows-alt""></i></span>
                                <span> @((await listItem.Item.Category.GetNewItems(Context.User.GetId())).ToString().WrapCategoryItem()) </span>
                            </div> j += 1;
                        }
                        </div>
                    </div>
                </div> i += 1;").IsDefault();

            // ================ Code Extensions: ================

            ViewModelProperty<Domain.Article>("Article").CacheValue().FromRequestParam("article");

            // ================ Buttons: ================

            Image("Add article").Name("AddArticle").Icon(FA.Plus)
                .OnClick(x =>
                {
                    x.Go<Settings.Categories.Articles.EnterPage>().SendReturnUrl()
                    .Send("cat", "listItem.Item?.CategoryId");
                });

            LoadJavascriptService("LearnPage");
        }
    }
}