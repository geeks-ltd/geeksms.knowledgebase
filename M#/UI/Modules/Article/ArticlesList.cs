using MSharp;

namespace Modules
{
    public class ArticlesList : MSharp.ListModule<Domain.Article>
    {
        public ArticlesList()
        {
            HeaderText("All articles")
                .IndexColumn()
                .Sortable();

            // ================ Search: ================

            Search(x => x.Category).AsCollapsibleCheckBoxList();

            Search(GeneralSearch.AllFields).Label("Find:").AfterInput("[#BUTTONS(Search)#]");

            SearchButton("Search")
            .OnClick(x =>
            {
                x.Reload();
            });

            // ================ Columns: ================

            Column(x => x.Category);

            Column(x => x.Subject);

            Column(x => x.ContentItems).DisplayExpression(cs("await item.ContentItems.Count()"))
                .SortingStatement("await item.ContentItems.Count()")
                .SortKey("ContentItemsCount");

            ButtonColumn("Edit").HeaderText("Edit").Icon(FA.Edit)
            .OnClick(x =>
            {
                x.Go<Settings.Categories.Articles.EnterPage>().SendReturnUrl()
                    .Send("cat", "item.Category.ID")
                    .Send("item", "item.ID");
            });

            ButtonColumn("DeleteCommand").HeaderText("Delete")
                .CssClass("btn btn-danger")
                .Text("Delete")
                .ConfirmQuestion("Are you sure you want to delete this Article?")
                .Icon(FA.Remove)
            .OnClick(x =>
            {
                x.DeleteItem();
                x.Reload();
            });

            Button("Add")
                .OnClick(x => x.Go<Settings.Categories.Articles.EnterPage>().SendReturnUrl());
        }
    }
}
