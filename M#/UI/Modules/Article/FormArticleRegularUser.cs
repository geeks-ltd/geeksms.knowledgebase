using MSharp;

namespace Modules
{
    public class FormArticleRegularUser : FormModule<Domain.Article>
    {
        /*M#:w[7]T-Prop:SupportsAdd-Type:FormModule-All links to this page seem to pass the Article ID. So this form appears to be used for 'Edit' only. In that case, set 'SupportsAdd' to 'false' to prevent errors or security breach.*/
        public FormArticleRegularUser()
        {
            HeaderText("Article details").SupportsEdit();

            Field(x => x.Category).AsAutoComplete();

            Field(x => x.Subject);

            AutoSet(x => x.CreatedBy).Value("await Context.Current.Person()");

            AutoSet(x => x.Category);

            // ================ Buttons: ================

            Button("Cancel")
                .OnClick(x => x.ReturnToPreviousPage());

            Button("Save").IsDefault()
            .OnClick(x =>
            {
                x.SaveInDatabase();

                x.ReturnToPreviousPage();
            });
        }
    }
}