using MSharp;

namespace Modules
{
    public class FormArticle : FormModule<Domain.Article>
    {
        public FormArticle()
        {
            HeaderText("Article details");

            Field(x => x.Category).AsAutoComplete();

            Field(x => x.Subject).Label("Title").AutoFocus();

            Field(x => x.Contents).ExtraControlAttributes("data-toolbar=\"Compact\"").NoLabel()
                .Mandatory(false)
                .NumberOfLines(20)
                .AsHtmlEditor();

            AutoSet(x => x.CreatedBy).Value("await Context.Current.Person()");

            AutoSet(x => x.Category).FromRequestParam("cat").SetInPostback(false);

            // ================ Buttons: ================

            Button("Cancel")
                .OnClick(x => x.ReturnToPreviousPage());

            Button("Save").IsDefault()
            .OnClick(x =>
            {
                x.SaveInDatabase();

                x.ReturnToPreviousPage();
            });

            // LoadJavascriptModule("scripts/ckEditorModule.js", absoluteUrl: true);
        }
    }
}