using MSharp;

namespace Modules
{
    public class ArticleList : MSharp.ListModule<Domain.Article>
    {
        public ArticleList()
        {
            HeaderText("Training category: <span class=\"text-muted\">@Model.Category </span> [#BUTTONS(Back)#][#BUTTONS(NewSubject)#]")
                .Sortable()
             .SourceCriteria("item.Category == info.Category");

            // ================ Search: ================

            Search(GeneralSearch.ClientSideFilter).WatermarkText("Filter...");

            // ================ Columns: ================

            Column(x => x.ContentItems).DisplayExpression(cs("await item.ContentItems.Count()"))
                .LabelText("Articles")
                .SortingStatement("await item.ContentItems.Count()")
                .SortKey("ContentItemsCount");

            LinkColumn("Subject").HeaderText("Subject")
                .Text(cs("item.Subject"))
                .SortKey("Subject")
                .OnClick(x => x.Go<LearnPage>()
                .Send("article", "item.ID")
                );

            ViewModelProperty<Domain.ArticleCategory>("Category").CacheValue().FromRequestParam("cat");

            // ================ Buttons: ================

            Button("Back")
                .CssClass("float-right")
                .OnClick(x => x.Go<Learn.CategoriesPage>());

            Button("New subject")
                .CssClass("btn btn-info float-right mr-1")
                .OnClick(x =>
                {
                    x.Go<Settings.Categories.Articles.EnterPage>().SendReturnUrl()
                        .Pass("cat", from: "cat");
                });
        }
    }
}