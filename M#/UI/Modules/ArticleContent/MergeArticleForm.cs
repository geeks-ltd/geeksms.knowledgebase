using MSharp;

namespace Modules
{
    public class MergeArticleForm : MSharp.FormModule<Domain.ArticleContent>
    {
        public MergeArticleForm()
        {
            HeaderText("Merge article")
                .SupportsAdd()
                .SupportsEdit(false);

            CustomField().Label("Article to delete:")
                .ControlMarkup("@Model.Article <b>which has</b> @await Model.Article.ContentItems.Count() <b> items </b>");

            Field(x => x.Article).Label("Move content items to:")
                .DisplayExpression("item.Category + \" > \" + item")
                .AsAutoComplete();

            ViewModelProperty<Domain.Article>("CurrentArticle").RetainInPost();

            OnBound_GET("Set article").Code(@"info.Article = await Request.Get<Article>(""article"");
info.CurrentArticle = info.Article;");

            // ================ Buttons: ================

            Button("Cancel")
                .OnClick(x => x.CloseModal());

            Button("Merge").IsDefault()
                .OnClick(x =>
                {
                    x.CSharp(@"await info.CurrentArticle.MergeWith(info.Article);").ValidationError();
                    x.CloseModal(Refresh.Full);
                });
        }
    }
}
