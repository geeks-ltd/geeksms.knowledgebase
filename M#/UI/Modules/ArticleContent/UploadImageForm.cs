namespace Modules
{
    public class UploadImageForm : MSharp.FormModule<Domain.ArticleContent>
    {
        public UploadImageForm()
        {
            HeaderText("Upload an image")
                .SupportsEdit(false)
                .Footer(@"<div>
            1. Right click below and select <b>Copy image</b>.<br/>
            [#BUTTONS(Preview)#]
            <br/>
            2. Close this popup.<br/>
            3. Paste the image where you want.
            </div>");

            Field(x => x.File).ReloadOnChange()
                .AfterControl("[#BUTTONS(Upload)#]");

            // ================ Buttons: ================           

            Button("Upload").IsDefault()
.CssClass("d-none")
.OnClick(x =>
{
    /*M#:w[33]T-Prop:Code-Type:GenericActivity-Do not hard code IMG tag directly. Instead define your image as an M# Button with ImageUrl property set.*/
    x.CSharp(@"var data = (await info.Item.File.GetFileDataAsync()).ToBase64String();
                var mime = ""image/"" + info.Item.File.FileExtension.Trim('.').ToLower();
                var url = $""data:{mime};base64, {data}"";
                var image = $""<img src=\""{url}\"" style='max-width: 800px; max-height: 400px' />"";
                ViewBag.ImagePath = url;
                ");

    x.RefreshPage();
});

            /*M#:w[48]T-Prop:ControlAttributes-Type:ModuleButton-Hardcoding in-line style is forbidden. Set a Css class instead.*/
            Image("Preview").ExtraTagAttributes("style='max-height:150px; max-width:600px'").ImageUrl(cs("ViewBag.ImagePath"));

            LoadJavascriptModule("/scripts/preview-image.js", absoluteUrl: true);
        }
    }
}