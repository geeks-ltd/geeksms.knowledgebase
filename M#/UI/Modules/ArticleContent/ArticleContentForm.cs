using MSharp;

namespace Modules
{
    public class ArticleContentForm : MSharp.FormModule<Domain.ArticleContent>
    {
        /*M#:w[7]T-Prop:SupportsAdd-Type:FormModule-All links to this page seem to pass the ArticleContent ID. So this form appears to be used for 'Edit' only. In that case, set 'SupportsAdd' to 'false' to prevent errors or security breach.*/
        public ArticleContentForm()
        {
            HeaderText("Article Content Details");

            Field(x => x.Article).AsAutoComplete();
            Field(x => x.Subject);
            Field(x => x.LastUpdated);
            Field(x => x.UpdatedBy).AsAutoComplete();
            Field(x => x.Body).Readonly();
            Field(x => x.Type).Readonly();
            Field(x => x.CollapsedInformation).Readonly();

            // ================ Buttons: ================

            Button("Cancel")
                .OnClick(x => x.ReturnToPreviousPage());

            Button("Save").IsDefault()
            .OnClick(x =>
            {
                x.SaveInDatabase();
                x.ReturnToPreviousPage();
            });
        }
    }
}