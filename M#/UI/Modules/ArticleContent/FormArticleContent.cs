using MSharp;

namespace Modules
{
    public class FormArticleContent : FormModule<Domain.ArticleContent>
    {
        /*M#:w[7]T-Prop:SupportsEdit-Type:FormModule-No page linking to this page seem to pass the ArticleContent ID. So this form appears to be used for 'Insert' only, while it allows editing. In that case, set 'SupportsEdit' to 'false' to prevent errors or security breach.*/
        public FormArticleContent()
        {
            SupportsAdd(true).SupportsEdit(true);

            var overviewBox = Box("Overview", BoxTemplate.HeaderBox);

            overviewBox.Add(Field(x => x.Article).DataSource("await Extension.GetArticleList()")
                .DisplayExpression("item.Category + \" > \" + item.Subject")
                .AsAutoComplete());

            overviewBox.Add(Field(x => x.Type).ReloadOnChange()
                .AsRadioButtons(Arrange.Horizontal));

            overviewBox.Add(Field(x => x.Subject).Label(cs("Model.Type.SubjectField +\":\""))
                .AutoFocus());

            var contentBox = Box("Content", BoxTemplate.HeaderBox);

            contentBox.Add(Field(x => x.Body)
                .VisibleIf("!info.Type.IsHtml")
                .Label(cs("Model.Type.BodyField +\":\""))
                .NumberOfLines(10));

            contentBox.Add(CustomField()
                .VisibleIf("info.Type.IsHtml")
                .NoLabel()
                .PropertyName("txtBody1")
                .BeforeControl("[#BUTTONS(Image)#]")
                .ExtraControlAttributes("data-toolbar=\"Compact\"")
                .AsHtmlEditor());

            var linkToOtherArticlesBox = Box("Link to other articles", BoxTemplate.HeaderBox);

            linkToOtherArticlesBox.Add(MasterDetail(x => x.Links, al =>
            {
                al.Orientation(Arrange.Vertical);

                al.Field(x => x.Content).NoLabel()
                    .DataSource("await Extension.GetArticleContentList()")
                    .DisplayExpression("item.FullName")
                    .AsAutoComplete();

                al.Link("Add article").Icon(FA.Plus).CausesValidation(false).OnClick(x => x.AddMasterDetailRow());
            }).NoLabel());

            var moreBox = Box("More", BoxTemplate.CollapsiblePanel);

            moreBox.Add(Field(x => x.File).Label("Image / video / File:"));

            moreBox.Add(Field(x => x.CollapsedInformation).ExtraControlAttributes("data-toolbar=\"Compact\"").AsHtmlEditor());

            AutoSet(x => x.Article).FromRequestParam("article").SetInPostback(false);

            AutoSet(x => x.Type).SetInPostback(false)
                .Value("info.Item.Type ?? ContentType.FAQ");

            AutoSet(x => x.UpdatedBy).Value("await Context.Current.Person()");

            // ================ Buttons: ================

            Button("Delete").ConfirmQuestion("Do you want to delete it?")
                .VisibleIf(CommonCriterion.IsEditMode_Item_IsNew)
                .OnClick(x =>
                {
                    x.DeleteItem();
                    x.ReturnToPreviousPage();
                });

            Button("Cancel").CausesValidation(false)
            .OnClick(x =>
            {
                x.ReturnToPreviousPage();
            });

            Button("Save").IsDefault()
            .OnClick(x =>
            {
                x.SaveInDatabase();
                x.ReturnToPreviousPage();
            });

            Link("Image").CssClass("btn btn-info mb-1").Text("Upload image...")
                .OnClick(x => x.PopUp<Learn.Content.UploadImagePage>());

            LoadJavascriptModule("scripts/ckEditorModule.js", absoluteUrl: true);
        }
    }
}