namespace Modules
{
    public class ListArticleContent : MSharp.ListModule<Domain.ArticleContent>
    {
        public ListArticleContent()
        {
            IsViewComponent().RenderMode(MSharp.ListRenderMode.List).UseAntiForgeryToken(false).WrapInForm(false);

            ShowHeaderRow(false)
                .RootCssClass("list-article-content")
                .DataSource("await info.Article.ContentItems.GetList()")
                .NoEmptyMarkup()
                .SourceEvaluationCriteria("info.Article != null")
                .SortingStatement("item.Order");

            // ================ Columns: ================
            Markup("@((await item.Render()).Raw())");

            // ================ Code Extensions: ================

            ViewModelProperty<Domain.Article>("Article").CacheValue().FromRequestParam("article");
        }
    }
}