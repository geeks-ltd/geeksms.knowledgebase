using MSharp;

namespace Modules
{
    public class ArticleContentsList : MSharp.ListModule<Domain.ArticleContent>
    {
        public ArticleContentsList()
        {
            HeaderText("Article Contents").Sortable();

            // ================ Search: ================

            Search(x => x.Article.Category).AsAutoComplete();

            Search(x => x.Article).AsAutoComplete();

            Search(GeneralSearch.ClientSideFilter).Label("Find:");

            SearchButton("Search")
            .OnClick(x =>
            {
                x.Reload();
            });

            // ================ Columns: ================

            Column(x => x.FullName).SortKey("FullName");

            LinkColumn("Reading history")
            .OnClick(x =>
            {
                x.Go<Settings.ArticleContents.ReadHistoryPage>()
                    .Send("content", "item.ID");
            });

            Column(x => x.Type);

            ButtonColumn("Edit").HeaderText("Edit")
                .GridColumnCssClass("actions")
                .Icon(FA.Edit)
            .OnClick(x =>
            {
                x.Go<Settings.ArticleContents.EnterPage>().SendReturnUrl()
                    .Send("item", "item.ID");
            });

            ImageColumn("DeleteCommand").HeaderText("Delete")
                .Text("Delete")
                .CssClass("btn btn-danger")
                .GridColumnCssClass("actions")
                .ConfirmQuestion("Are you sure you want to delete this Article content?")
                .Icon(FA.Remove)
            .OnClick(x =>
            {
                x.DeleteItem();
                x.Reload();
            });
        }
    }
}