namespace Modules
{
    public class CategoryArticlesRender : MSharp.ListModule<Domain.ArticleContent>
    {
        public CategoryArticlesRender()
        {
            HeaderText(cs("Model.Category"))
                .ShowHeaderRow(false)
                .Sortable()
                .DataSource("await (await info.Category.Articles.GetList()).SelectManyAsync(x => x.ContentItems.GetList())")
                .NoEmptyMarkup()
                .RootCssClass("article-contents");

            // ================ Columns: ================

            CustomColumn()
                .DisplayExpression(cs("(await item.Render()).Raw()"))
                .IsSortable(false)
                .LabelText("Markup");

            // ================ Grouping options: ================

            Grouping(x => x.Article).IsDefault();

            ViewModelProperty<Domain.ArticleCategory>("Category").CacheValue().FromRequestParam("id");
        }
    }
}