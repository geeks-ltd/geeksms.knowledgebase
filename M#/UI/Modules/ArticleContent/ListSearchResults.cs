using MSharp;

namespace Modules
{
    public class ListSearchResults : ListModule<Domain.ArticleContent>
    {
        public ListSearchResults()
        {
            HeaderText("Full search through the training articles")
                .Sortable()
                .StartUpBehaviour(OnStart.WaitForSearch)
                .DataSource("await (await Context.Current.Person()).GetMyContents()");

            // ================ Search: ================

            Search(GeneralSearch.AllFields).NoLabel()
                .AfterInput("[#BUTTONS(Search)#]")
                .WatermarkText("Search...");

            SearchButton("Search")
            .OnClick(x =>
            {
                x.Reload();
            });

            // ================ Columns: ================

            Column(x => x.Article.Category);

            LinkColumn("FullName").HeaderText("Article")
                .Text(cs("item.FullName"))
                .SortKey("FullName")
                .MarkupTemplate(@"[#Button#]<br/>@item.Body.RemoveHtmlTags().Summarize(200)")
                .OnClick(x => x.Go<LearnPage>().Send("article", "item.ArticleId"));
        }
    }
}