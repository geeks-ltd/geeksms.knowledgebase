using MSharp;

namespace Modules
{
    public class ListArticleCategory : ListModule<Domain.ArticleCategory>
    {
        public ListArticleCategory()
        {
            HeaderText("Categories")
                .Sortable()
                .SortingStatement("item.Order");

            // ================ Search: ================

            Search(GeneralSearch.AllFields).Label("Find:").AfterInput("[#BUTTONS(Search)#]");

            SearchButton("Search")
            .OnClick(x =>
            {
                x.Reload();
            });

            // ================ Columns: ================

            LinkColumn("Category").HeaderText("Name")
                .Text(cs("item.Name"))
                .SortKey("Name")
                .OnClick(x =>
                {
                    x.Go<Settings.Categories.ArticlesPage>()
                    .Send("id", "item.ID");
                });

            Column(x => x.Articles).DisplayExpression(cs("await item.Articles.Count()"))
                .SortingStatement("await item.Articles.Count()")
                .SortKey("ArticlesCount");

            Column(x => x.AccessLevels).DisplayExpression(cs("(await item.AccessLevels.GetList()).ToHtmlLines().Raw()"))
                .GridColumnCssClass("no-wrap")
                .SortingStatement("await item.AccessLevels.Count()")
                .SortKey("AccessLevelsCount");

            ButtonColumn("Move up").HeaderText("Order")
                .CssClass("btn btn-info")
                .Icon(FA.ArrowCircleUp)
            .OnClick(x =>
            {
                x.MoveUp();
                x.Reload();
            });

            ButtonColumn("Move down").HeaderText("Order")
                .CssClass("btn btn-info")
                .Icon(FA.ArrowCircleDown)
            .OnClick(x =>
            {
                x.MoveDown();
                x.Reload();
            });

            ButtonColumn("Edit").HeaderText("Edit")
                .Icon(FA.Edit)
            .OnClick(x =>
            {
                x.Go<Settings.Categories.EnterPage>().SendReturnUrl()
                    .Send("item", "item.ID");
            });

            ButtonColumn("DeleteCommand").HeaderText("Delete")
                .Text("Delete")
                .CssClass("btn btn-danger")
                .ConfirmQuestion("Are you sure you want to delete this Article category?")
                .Icon(FA.Remove)
            .OnClick(x =>
            {
                x.DeleteItem();
                x.Reload();
            });

            ButtonColumn("PDF")
            .OnClick(x =>
            {
                x.Go<Settings.Categories.RenderPDFPage>()
                    .Send("id", "item.ID");
            });

            Button("Add")
                .OnClick(x => x.Go<Settings.Categories.EnterPage>().SendReturnUrl());
        }
    }
}