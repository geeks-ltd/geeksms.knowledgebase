using MSharp;

namespace Modules
{
    public class FormArticleCategory : FormModule<Domain.ArticleCategory>
    {
        public FormArticleCategory()
        {
            HeaderText("Category details");

            Field(x => x.Name).Label("Category name:");
            Field(x => x.Order);
            Field(x => x.Microsites);

            var accessBox = Box("Access", BoxTemplate.HeaderBox);

            accessBox.Add(MasterDetail(x => x.AccessLevels, al =>
            {
                al.Orientation(Arrange.Vertical);

                al.Field(x => x.Level).Label("From Level:").WatermarkText("Any");
                al.Field(x => x.Role);
                al.Field(x => x.Access);
                al.Link("Access level").Icon(FA.Plus).CausesValidation(false).OnClick(x => x.AddMasterDetailRow());
            }).NoLabel());

            // ================ Buttons: ================

            Button("Cancel")
                .OnClick(x => x.ReturnToPreviousPage());

            Button("Save").IsDefault()
            .OnClick(x =>
            {
                x.SaveInDatabase();
                x.If("await info.Item.Articles.None()").CSharp("await info.Item.SaveTodoArticle(await Context.Current.Person());");
                x.ReturnToPreviousPage();
            });
        }
    }
}