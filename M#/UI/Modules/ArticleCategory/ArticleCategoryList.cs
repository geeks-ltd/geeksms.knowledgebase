using MSharp;

namespace Modules
{
    public class ArticleCategoryList : MSharp.ListModule<Domain.ArticleCategory>
    {
        public ArticleCategoryList()
        {
            HeaderText("My Training Categories")
                .Sortable();

            DataSource("await GetItems()");

            // ================ Search: ================

            Search(GeneralSearch.ClientSideFilter).WatermarkText("Filter...");

            // ================ Columns: ================

            Column(x => x.Name);

            LinkColumn("Articles").Text(cs("await item.Articles.Count() +\" subjects »\""))
            .OnClick(x =>
            {
                x.Go<Learn.ArticlesPage>()
                    .Send("cat", "item.ID");
            });
        }
    }
}