namespace Modules
{
    public class ReadHistoryList : MSharp.ListModule<Domain.ArticleContentStudy>
    {
        public ReadHistoryList()
        {
            HeaderText("c#:\"Read History: \" + Model.ArticleContent.FullName")
                .Sortable()
                .DataSource("await info.ArticleContent.Studies.GetList()");

            // ================ Columns: ================

            Column(x => x.User);

            Column(x => x.Updated).LabelText("Date");

            ViewModelProperty<Domain.ArticleContent>("ArticleContent").CacheValue().FromRequestParam("content");
        }
    }
}