using MSharp;
using PeopleService;

namespace Modules
{
    public class ArticleContentStudyList : MSharp.ListModule<Domain.ArticleContentStudy>
    {
        public ArticleContentStudyList()
        {
            HeaderText("Article reading history")
                .Sortable()
                .DataSource("await (await Context.Current.Person()).ArticleContentStudies()");

            // ================ Search: ================

            Search(x => x.Article.Category);

            Search(x => x.Content).AsAutoComplete();

            SearchButton("Search")
            .OnClick(x =>
            {
                x.Reload();
            });

            // ================ Columns: ================

            Column(x => x.Updated).LabelText("Date");

            Column(x => x.Article.Category);

            LinkColumn("FullName").HeaderText("Article")
.Text(cs("item.Content.FullName"))
.SortingStatement("await item.Content.FullName")
.SortKey("Article")
.OnClick(x =>
{
    x.Go(cs("item.Content.GetUrl()"))
    .Send("id", "Item.ID");
});

            ViewModelProperty<Person>("User").CacheValue().FromRequestParam("Person");
        }
    }
}