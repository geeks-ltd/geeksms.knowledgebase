using MSharp;
using PeopleService;

namespace Domain
{
    public class ArticleContent : EntityType
    {
        public ArticleContent()
        {
            Sortable();

            BigString("Subject", 2).Lines(2);

            BigString("Body", 4).Lines(4);

            BigString("Collapsed information", 10).Lines(10);

            OpenImage("File").AutoOptimize(preserveTransparency: false, maxWidth: 1200, maxHeight: 1200);

            String("Full name").CalculatedFrom("Article +\" > \" + Subject");

            Int("Ref").Mandatory().AutoNumber();

            DateTime("Last updated").Mandatory();

            Associate<ContentType>("Type").Mandatory();

            Associate<Article>("Article").Mandatory().DatabaseIndex();

            Associate<Person>("Updated by");

            InverseAssociate<ArticleContentLink>("Links", inverseOf: "Owner");

            InverseAssociate<ArticleContentStudy>("Studies", inverseOf: "Content");

            DefaultSort = Int("Order").Mandatory();
        }
    }
}