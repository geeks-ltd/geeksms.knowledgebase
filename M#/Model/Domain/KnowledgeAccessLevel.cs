using MSharp;

namespace Domain
{
    public class KnowledgeAccessLevel : EntityType
    {
        public KnowledgeAccessLevel()
        {
            InstanceAccessors("Edit", "MandatoryRead");

            String("Name").Mandatory();
        }
    }
}