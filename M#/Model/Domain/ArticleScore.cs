using MSharp;

namespace Domain
{
    public class ArticleScore : EntityType
    {
        public ArticleScore()
        {
            String("Name").Mandatory();

            Int("Score").Mandatory();
        }
    }
}