using MSharp;
using PeopleService;

namespace Domain
{
    public class ArticleContentStudy : EntityType
    {
        public ArticleContentStudy()
        {
            DateTime("Updated").Mandatory();

            Associate<ArticleContent>("Content").Mandatory()
                .DatabaseIndex()
                .OnDelete(CascadeAction.CascadeDelete);

            Associate<Person>("User");

            Associate<Article>("Article").Calculated().CalculatedFrom("Content.Article");

            UniqueCombination(new[] { "Content", "User" });
        }
    }
}