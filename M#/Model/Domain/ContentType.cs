using MSharp;

namespace Domain
{
    public class ContentType : EntityType
    {
        public ContentType()
        {
            InstanceAccessors("FAQ", "Glossary", "Tip", "Tasks", "Custom").IsEnumReference().Sortable();

            String("Name", 100).Mandatory().Unique();

            String("Subject field").Mandatory();

            String("Body field").Mandatory();

            Bool("Is html").Mandatory();

            DefaultSort = Int("Order").Mandatory();
        }
    }
}