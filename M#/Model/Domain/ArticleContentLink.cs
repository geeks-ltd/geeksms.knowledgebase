using MSharp;

namespace Domain
{
    public class ArticleContentLink : EntityType
    {
        public ArticleContentLink()
        {
            Associate<ArticleContent>("Owner").Mandatory()
                .DatabaseIndex()
                .OnDelete(CascadeAction.CascadeDelete);

            Associate<ArticleContent>("Content").Mandatory()
                .OnDelete(CascadeAction.CascadeDelete);
        }
    }
}