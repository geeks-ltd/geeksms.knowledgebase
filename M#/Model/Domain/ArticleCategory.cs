using MSharp;
using PeopleService;

namespace Domain
{
    public class ArticleCategory : EntityType
    {
        public ArticleCategory()
        {
            Sortable();

            String("Name").Mandatory();

            String("Microsites");

            Associate<Person>("Created by");

            InverseAssociate<Article>("Articles", inverseOf: "Category");

            InverseAssociate<KnowledgeAccess>("Access levels", inverseOf: "Category");

            DefaultSort = Int("Order").Mandatory();
        }
    }
}