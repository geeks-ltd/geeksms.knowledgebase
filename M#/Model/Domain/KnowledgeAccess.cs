using MSharp;
using PeopleService;

namespace Domain
{
    public class KnowledgeAccess : EntityType
    {
        public KnowledgeAccess()
        {
            // String("Level");

            ToStringExpression("Level.ToStringOrEmpty().Or(\"Any \") + Role + \": \" + Access");

            Associate<ArticleCategory>("Category").Mandatory().DatabaseIndex();

            Associate<KnowledgeAccessLevel>("Access").Mandatory();

            Associate<Role>("Role");

            Associate<Level>("Level");
        }
    }
}