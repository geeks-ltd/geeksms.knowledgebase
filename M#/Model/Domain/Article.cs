using MSharp;
using PeopleService;

namespace Domain
{
    public class Article : EntityType
    {
        public Article()
        {
            Sortable();

            String("Subject").Mandatory();

            BigString("Contents", 10).Lines(10);

            DateTime("Last updated date");

            Associate<Person>("Created by");

            Associate<Person>("Last updated by");

            Associate<ArticleCategory>("Category").Mandatory().DatabaseIndex();

            InverseAssociate<ArticleContent>("Content items", inverseOf: "Article");

            Int("Reference").Mandatory().AutoNumber();

            DefaultSort = Int("Order").Mandatory().Default("0");
        }
    }
}