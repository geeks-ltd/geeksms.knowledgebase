﻿import 'jquery-ui/ui/widgets/sortable';
import Url from "olive/components/url";
import Alert from "olive/components/alert";

export default class LearnPage implements IService {

    constructor(private url: Url, private alert: Alert) { }

    public run(): void {

        $(".card-body").sortable({
            handle: ".sort",
            stop: (event, ui) => {
                var currentId = $(ui.item).attr("data-a-id");
                var siblingId = $(ui.item).next().attr("data-a-id");

                this.changeArticleOrder({ id: currentId, siblingId: siblingId }, this.url.effectiveUrlProvider("/learn/change-article-order", null));
            }
        });

        $(".list-items").sortable({
            handle: ".sort",
            stop: (event, ui) => {

                var currentId = $(ui.item).find(".subject").attr("data-ac-id");
                var siblingId = $(ui.item).next().find(".subject").attr("data-ac-id");

                this.changeArticleOrder({ id: currentId, siblingId: siblingId }, this.url.effectiveUrlProvider("/learn/change-content-order", null));
            }
        });
    }

    changeArticleOrder(params, path) {

        $.ajax({
            url: path,
            type: "POST",
            xhrFields: { withCredentials: true },
            data: params,
            success: (response) => {
                this.alert.alertUnobtrusively("Item moved to a new position.");
            },
            error: (event) => {
                alert("Error in changing order ");
            }
        });
    }
}