define(["require", "exports", "olive/di/services", "./learnPage.js"], function (require, exports, services_1, learnPage_js_1) {
    Object.defineProperty(exports, "__esModule", { value: true });
    class ConfigureServices {
        static configureServices(services) {
            services.addSingleton("LearnPage", (url, alert) => new learnPage_js_1.default(url, alert)).withDependencies(services_1.default.Url, services_1.default.Alert);
        }
    }
    exports.default = ConfigureServices;
    ConfigureServices.configureServices(window.page.services);
});
//# sourceMappingURL=configureServices.js.map