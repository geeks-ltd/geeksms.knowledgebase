﻿import OlivePage from "olive/olivePage";
import { ServiceContainer } from "olive/di/serviceContainer";
import Services from "olive/di/services";
import LearnPage from "./learnPage.js";
import Url from "olive/components/url";
import Alert from "olive/components/alert";

export default class ConfigureServices {

    public static configureServices(services: ServiceContainer) {
        services.addSingleton("LearnPage", (url: Url, alert: Alert) => new LearnPage(url, alert)).withDependencies(Services.Url, Services.Alert);
    }
}

ConfigureServices.configureServices((<OlivePage>window.page).services);