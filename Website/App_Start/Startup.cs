﻿namespace Website
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Olive;
    using Olive.Entities.Data;
    using Olive.Hangfire;
    using Olive.Mvc.Microservices;
    using System;
    using System.Globalization;

    public abstract class Startup : Olive.Mvc.Microservices.Startup
    {
        public Startup(IHostingEnvironment env, IConfiguration config, ILoggerFactory loggerFactory) : base(env, config, loggerFactory)
        {
            SetUpIdentity(env, config);
        }

        protected abstract void SetUpIdentity(IHostingEnvironment env, IConfiguration config);

        protected override CultureInfo GetRequestCulture() => new CultureInfo("en-GB");

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddTSConfiguration();
            services.AddDataAccess(x => x.SqlServer());
            services.AddScheduledTasks();
            services.AddSwagger();
        }

        public override void Configure(IApplicationBuilder app)
        {
            base.Configure(app);
            app.ConfigureSwagger();
            Console.Title = Microservice.Me.Name;
        }
        protected override void ConfigureExceptionPage(IApplicationBuilder app) => app.UseDeveloperExceptionPage();

    }
}