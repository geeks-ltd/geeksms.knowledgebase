﻿using Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Olive;
using Olive.Entities.Data;
using Olive.Hangfire;
using Olive.Mvc.Testing;
using System.Threading.Tasks;

namespace Website
{
    public class StartupDevelopment : Startup
    {
        public StartupDevelopment(IHostingEnvironment env, IConfiguration config, ILoggerFactory factory) : base(env, config, factory)
        {
        }

        protected override void SetUpIdentity(IHostingEnvironment env, IConfiguration config)
        {
            config.LoadAwsDevIdentity();
        }

        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);

            services.AddDevCommands(x => x.AddTempDatabase<SqlServerManager, ReferenceData>().AddClearApiCache());
            services.AddIOEventBus();
        }

        public override async Task OnStartUpAsync(IApplicationBuilder app)
        {
            await base.OnStartUpAsync(app);
            app.UseScheduledTasks<TaskManager>();
            await new PeopleService.KnowledgeBaseEndPoint(typeof(PeopleService.Person).Assembly).Subscribe();
        }
    }
}