﻿using Microsoft.AspNetCore.Hosting;
using Olive;
using Olive.Logging;

namespace Website
{
    public class LambdaFunction : Amazon.Lambda.AspNetCoreServer.ApplicationLoadBalancerFunction
    {
        protected override void Init(IWebHostBuilder builder)
        {
            var start = LocalTime.Now;
            RegisterResponseContentEncodingForContentType("font/woff2", Amazon.Lambda.AspNetCoreServer.ResponseContentEncoding.Base64);
            builder.ConfigureLogging(p => p.AddEventBus()).UseStartup(typeof(Startup).Assembly.FullName);
        }
    }
}