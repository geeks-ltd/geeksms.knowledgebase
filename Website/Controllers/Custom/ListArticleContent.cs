﻿using Domain;
using Microsoft.AspNetCore.Mvc;
using Olive;
using Olive.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ViewModel
{
    public partial class ListArticleContent
    {
        public async Task<bool> CanEdit()
        {
            return Article != null && await (await Context.Current.Person()).CanEdit(Article.Category);
        }
    }
}