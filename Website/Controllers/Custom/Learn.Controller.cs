﻿using Domain;
using Microsoft.AspNetCore.Mvc;
using Olive;
using Olive.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Controllers
{
    public partial class LearnController
    {
        [HttpPost, Route("learn/change-article-order")]
        public async Task<ActionResult> ChangeOrderOfArticle(PickedListChangeRequest data)
        {
            var selectedArticle = await Database.Get<Article>(data.Id);

            if (data.SiblingId.HasValue)
            {
                var siblingArticle = await Database.Get<Article>(data.SiblingId);

                await Sorter.MoveBefore(selectedArticle, siblingArticle);
            }
            else
            {
                await Sorter.MoveLast(selectedArticle);
            }

            return JsonActions();
        }

        [HttpPost, Route("learn/change-content-order")]
        public async Task<ActionResult> ChangeOrderOfArticleContent(PickedListChangeRequest data)
        {
            var selectedArticle = await Database.Get<ArticleContent>(data.Id);

            if (data.SiblingId.HasValue)
            {
                var siblingArticle = await Database.Get<ArticleContent>(data.SiblingId);

                await Sorter.MoveBefore(selectedArticle, siblingArticle);
            }
            else
            {
                await Sorter.MoveLast(selectedArticle);
            }

            return JsonActions();
        }
    }

    public class PickedListChangeRequest
    {
        public Guid Id { get; set; }

        public Guid? SiblingId { get; set; }
    }
}

namespace ViewModel
{
    public partial class ViewArticle
    {
        public async Task<IEnumerable<Article>> Studies() => await (await Context.Current.Person()).GetMyArticles();

        public async Task<IEnumerable<ArticleCategory>> StudyCategories() => await Studies().Select(x => x.Category);
    }
}