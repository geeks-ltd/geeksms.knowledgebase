﻿using Domain;
using Olive;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Controllers
{
    public partial class LearnCategoriesController
    {
        private async Task<IEnumerable<ArticleCategory>> GetItems()
        {
            var person = await Context.Current.Person();

            return await Database.Of<ArticleCategory>().GetList().WhereAsync(x => person.CanSee(x));
        }
    }
}