﻿using Olive;
using Olive.Entities;
using Olive.Entities.Data;
using System.Threading.Tasks;

namespace Domain
{
    public class ReferenceData : IReferenceData
    {
        IDatabase Database;
        public ReferenceData(IDatabase database) => Database = database;
        async Task<T> Create<T>(T item) where T : IEntity
        {
            await Context.Current.Database().Save(item, SaveBehaviour.BypassAll);
            return item;
        }

        public async Task Create()
        {
            await CreateContentType();
            await CreateKnowledgeAccessLevel();
        }

        async Task CreateContentType()
        {
            await Create(new ContentType
            {
                Name = "FAQ",
                BodyField = "Answer",
                SubjectField = "Question",
                IsHtml = false,
                Order = 0
            });

            await Create(new ContentType
            {
                Name = "Glossary",
                BodyField = "Definition",
                SubjectField = "Term",
                IsHtml = false,
                Order = 1
            });

            await Create(new ContentType
            {
                Name = "Tip",
                BodyField = "Tip",
                SubjectField = "Title",
                IsHtml = false,
                Order = 2
            });

            await Create(new ContentType
            {
                Name = "Tasks",
                BodyField = "Body field",
                SubjectField = "Title",
                IsHtml = true,
                Order = 3
            });

            await Create(new ContentType
            {
                Name = "Custom",
                BodyField = "Body field",
                SubjectField = "Name",
                IsHtml = true,
                Order = 4
            });
        }

        async Task CreateKnowledgeAccessLevel()
        {
            await Create(new KnowledgeAccessLevel { Name = "Edit" });
            await Create(new KnowledgeAccessLevel { Name = "MandatoryRead" });
        }
    }
}