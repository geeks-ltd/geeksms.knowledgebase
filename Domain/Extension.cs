﻿using EmailService;
using Olive;
using Olive.Entities;
using Olive.Web;
using PeopleService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public static class Extension
    {
        const int CHRISTMAS = 12;

        static IDatabase Database => Context.Current.Database();

        public static bool IsInRole(this Person @this, string roleName) => @this.Roles.Split(",").Contains(roleName, caseSensitive: false);

        public static async Task<Person> Person(this Context @this)
        {
            var userId = @this.Http().User.GetId().TryParseAs<Guid>() ?? throw new Exception("No current user!");

            if (!await @this.Database().Any<Person>(x => x.ID == userId))
                return null;

            return await @this.Database().Get<Person>(userId);
        }

        public static Task<IEnumerable<Person>> ActiveUsers()
        {
            return Context.Current.Database().GetList<Person>(x => x.IsActive);
        }

        public static bool IsOutsideWorkingHours(this DateTime time)
        {
            if (time.Hour < 9) return true;
            if (time.Hour >= 18) return true;
            if (time.IsGeeksDayOff()) return true;

            return false;
        }

        public static bool IsGeeksDayOff(this DateTime day)
        {
            if (day.IsEnglishHoliday()) return true;

            if (day.Month == (int)CalendarMonth.December && day.Day >= CHRISTMAS) return true;

            if (day.Month == 1 && day.Day == 1) return true;

            return false;
        }

        public static Task<bool> CanEdit(this Person @this, ArticleCategory category)
        {
            if (category == null) return Task.FromResult<bool>(false);

            return HttpContextCache.GetOrAdd($"{@this.ID}CanEdit{category.ID}", async () =>
             {
                 if ((await Context.Current.Person()).IsInRole("Director")) return true;

                 if ((await category.AccessLevels.GetList()).Any(x => x.Allows(@this) && x.Access == KnowledgeAccessLevel.Edit))
                     return true;

                 return false;
             });
        }

        public static Task<bool> CanSee(this Person @this, ArticleCategory category)
        {
            if (@this == null)
                return Task.FromResult(false);

            return HttpContextCache.GetOrAdd($"{@this.ID}CanSee{category.ID}", async () =>
             {
                 if (await @this.CanEdit(category)) return true;

                 if ((await category.AccessLevels.GetList()).Any(x => x.Allows(@this))) return true;

                 return false;
             });
        }

        public static async Task<IEnumerable<Article>> GetArticleList()
        {
            var person = await Person(Context.Current);

            return await Database.GetList<Article>().WhereAsync(s => person.CanSee(s.Category));
        }

        public static async Task<IEnumerable<ArticleContent>> GetArticleContentList()
        {
            var person = await Person(Context.Current);

            return await Database.GetList<ArticleContent>().WhereAsync(s => person.CanSee(s.Article.Category));
        }

        public static async Task<IEnumerable<Article>> GetMyArticles(this Person @this)
        {
            return await (await Database.GetList<ArticleCategory>().WhereAsync(c => CanSee(@this, c))).SelectManyAsync(x => x.Articles.GetList());
        }

        public static async Task<string> GenerateArticleTitle(this Article @this)
        {
            var items = await @this.ContentItems.Where(x => x.Subject.IsEmpty()).GetList();

            var emptyItemCount = items.Count().ToString().Unless("0").WithWrappers("<div class=\"incomplete-items\">", "</div>");

            var currentPerson = await Context.Current.Person();

            var itemCount = (await @this.ContentItems.GetList()
                .WhereAsync(x => x.IsUpdatedFor(currentPerson)).Count()).ToString().Unless("0").WithWrappers("<div class=\"updated-items\">", "</div>");

            return $"{@this.Subject}{emptyItemCount}{itemCount}";
        }

        public static async Task<IEnumerable<ArticleContent>> GetMyContents(this Person @this)
        {
            var result = await (await Database.GetList<ArticleCategory>().WhereAsync(x => @this.CanSee(x))).SelectManyAsync(a => a.Articles.GetList());

            return await result.SelectManyAsync(s => s.ContentItems.GetList());
        }

        public static Task<IEnumerable<ArticleContentStudy>> ArticleContentStudies(this Person @this)
        {
            return Database.GetList<ArticleContentStudy>(x => x.UserId == @this);
        }

        public static string WrapCategoryItem(this string categoryCount)
        {
            return categoryCount.Unless("0").WithSuffix(" new").WithWrappers("<div class='progress'>", "</div>");
        }

        public static async Task SendMonthlyEmail()
        {
            var currentDate = LocalTime.Now;

            var firstDayOfMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

            if (currentDate.Equals(firstDayOfMonth))
            {
                var startTime = firstDayOfMonth.AddMonths(-1);

                var articles = await Database.Of<Article>()
                    .Where(x => x.LastUpdatedDate >= startTime && x.LastUpdatedDate <= firstDayOfMonth).GetList();

                var roles = await Database.Of<Role>().GetList();
                var levels = await Database.Of<Level>().GetList();

                foreach (var level in levels)
                {
                    foreach (var role in roles)
                    {
                        var articleList = new HashSet<Article>();

                        foreach (var article in articles)
                        {
                            if (await article.Category.AccessLevels.Any(x => x.LevelId == level.ID && x.RoleId == role.ID))
                            {
                                articleList.Add(article);
                            }
                        }

                        var userList = await Database.Of<Person>()
                            .Where(x => x.Roles.Contains($"{level.Name}{role.Name}")).GetList();

                        await SendEmail(userList, articleList);
                    }
                }
            }
        }

        private static async Task SendEmail(IEnumerable<Person> userList, HashSet<Article> articleList)
        {
            foreach (var receiver in userList)
            {
                var articleBody = new StringBuilder();
                var i = 0;

                var articleUrl = Microservice.Of("Hub").Url("knowledgebase/learn?article=");

                foreach (var article in articleList)
                {
                    i++;
                    articleBody.AppendFormat("{0}<a href='{1}'>{2}</a>", i, articleUrl + article.ID, article.Subject);
                }

                await new SendEmailCommand
                {
                    FromName = "Knowledge MS",
                    To = receiver.Email,
                    Html = true,
                    Subject = $"Monthly articles update: {articleList.Count} articles",
                    Body = $@"
                    Hi {receiver} <br/><br/>
                    {articleBody.ToString()}"
                }.Publish();
            }
        }
    }
}