﻿namespace Domain
{
    using EmailService;
    using Olive;
    using PeopleService;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides the business logic for ArticleCategory class.
    /// </summary>
    partial class ArticleCategory
    {
        const int MIN_ACTUAL_ARTICLE_LENGTH = 200;

        public async Task SaveTodoArticle(Person user)
        {
            await Database.Save(new Article
            {
                Category = this,
                Contents = "Update me!",
                LastUpdatedBy = user,
                LastUpdatedDate = LocalTime.Now,
                Order = 0,
                Subject = "TODO",
                CreatedBy = user
            });
        }

        public Task<bool> IsMandatoryFor(Person viewer)
        {
            // TODO: && x.Allows(viewer) Must be added.
            return AccessLevels.Any(x => x.Access == KnowledgeAccessLevel.MandatoryRead);
        }

        async Task<IEnumerable<ArticleContent>> FindNewItems(Person trainee)
        {
            if (!await IsMandatoryFor(trainee))
                return Enumerable.Empty<ArticleContent>();

            return await (await Articles.GetList()).SelectManyAsync(a => a.ContentItems.GetList()).WhereAsync(i => i.IsUpdatedFor(trainee));
        }

        public async Task<int> GetNewItems(Person trainee) => await FindNewItems(trainee).Count();

        public async Task<int> GetNewItems(string id)
        {
            var person = await Database.Get<Person>(id.To<Guid>());

            return await FindNewItems(person).Count();
        }

        public static async Task ChaseMandatoryReading()
        {
            if (LocalTime.Now.Hour < 16 || LocalTime.Now.IsOutsideWorkingHours()) return;
            // TODO:
            // if (Settings.Current.LastUnreadArticlesChased > LocalTime.Today.AddWorkingDays(-5)) return;

            // TODO:
            // Database.Update(Settings.Current, x => x.LastUnreadArticlesChased = LocalTime.Now);

            await (await Extension.ActiveUsers()).Do(ChaseMandatoryReading);
        }

        protected override async Task OnValidating(EventArgs e)
        {
            if (CreatedById == null || CreatedById == Guid.Empty)
            {
                CreatedBy = await Olive.Context.Current.Person();
            }

            await base.OnValidating(e);
        }

        static async Task ChaseMandatoryReading(Person user)
        {
            var mandatoryCategories = await Database.GetList<ArticleCategory>()
                .WhereAsync(x => x.IsMandatoryFor(user)).ToList();

            var toRead = await mandatoryCategories
                .SelectAsync(async c => new
                {
                    Category = c,
                    Unread = await c.FindNewItems(user).Where(i => i.Body.OrEmpty().RemoveHtmlTags().Length >= MIN_ACTUAL_ARTICLE_LENGTH).ToList()
                })
                .Where(x => x.Unread.HasMany()).ToList();

            if (toRead.Sum(x => x.Unread.Count) == 0) return;

            var name = user.Name.Split(' ').First();

            await new SendEmailCommand
            {
                FromName = "Teacher",
                To = user.Email,
                Html = true,
                Subject = $"{name}, new articles for you",
                Body = $@"<img style='float:right; max-height:200px' src='https://training.geeksltd.co.uk/images/knowledge.png'/>
Hi {name}<br/><br/>
There are key new training articles for you to ensure your knowledge remains up to date.<br/><br/>

Please read them when you get a chance and click 'Confirm Read' for each one after you've learnt it.<br/><br/>" +

                       toRead.Select(x => $"<a href='https://training.geeksltd.co.uk'>{x.Category}: {x.Unread.Count} articles</a>").ToString("<br/>") + "<br/><br/>" +

                       "These articles are written for you by:<br/><br/>" +
                       toRead.SelectMany(x => x.Unread).Select(x => x.UpdatedBy).Distinct().Where(x => x.IsActive).Take(6).Select(u => $"<img style='max-height:60px' src='{u.ImageUrl}'/>").ToLinesString()
            }.Publish();
        }
    }
}