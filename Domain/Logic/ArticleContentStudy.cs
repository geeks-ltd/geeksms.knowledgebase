﻿namespace Domain
{
    using Olive;
    using PeopleService;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides the business logic for ArticleContentStudy class.
    /// </summary>
    partial class ArticleContentStudy
    {
        public static async Task ConfirmRead(Person user, ArticleContent content)
        {
            var existing = await FindByContentAndUser(content, user);

            if (existing != null)
            {
                await Database.Update(existing, n => { n.Updated = LocalTime.Now; });
            }
            else
            {
                await Database.Save(new ArticleContentStudy
                {
                    Updated = LocalTime.Now,
                    Content = content,
                    User = user
                });
            }
        }
    }
}