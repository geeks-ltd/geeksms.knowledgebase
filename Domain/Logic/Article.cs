﻿namespace Domain
{
    using Olive;
    using Olive.Entities;
    using PeopleService;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides the business logic for Article class.
    /// </summary>
    public partial class Article
    {
        public async Task<IEnumerable<Article>> GetSiblings() => await Category.Articles.GetList();

        public static Task<IEnumerable<Person>> ActiveUsers() => Database.GetList<Person>(i => i.IsActive);

        //public string GetUrl() => "/a/" + Reference.ToBase32(); // Add back once we have the friendly url implemented

        public string GetUrl() => $"/knowledgebase/learn?article={ID}";

        public async Task MergeWith(Article another)
        {
            if (another.IsAnyOf(this, null))
                throw new ValidationException("Please select a different article to merge.");

            foreach (var item in await ContentItems.GetList())
            {
                await Database.Update(item, x => { x.Article = another; x.Order += 10000; });
            }

            var first = await another.ContentItems.FirstOrDefault();

            if (first != null) await Sorter.JustifyOrders(first);

            await Database.Delete(this);
        }

        protected override async Task OnValidating(EventArgs e)
        {
            await base.OnValidating(e);

            var contents = Contents + Subject;

            if (contents.Contains("<script"))
            {
                // await EmailApi.Fresh().Send(new Email { Subject = "Hack attempt", Body = LastUpdatedBy + " tried to submit the code: " + contents });

                throw new ValidationException("Error in the application.");
            }
        }
    }
}