﻿namespace Domain
{
    using PeopleService;

    /// <summary>
    /// Provides the business logic for KnowledgeAccess class.
    /// </summary>
    partial class KnowledgeAccess
    {
        internal bool Allows(Person user) => user.IsInRole($"{Level}{Role}");
    }
}