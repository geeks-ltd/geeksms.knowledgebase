﻿namespace Domain
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Routing;
    using Olive;
    using Olive.Entities;
    using Olive.Mvc;
    using PeopleService;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides the business logic for ArticleContent class.
    /// </summary>
    public partial class ArticleContent
    {
        const int MINIMUM_CONTENT_TO_COUNT_WRITTEN = 50;
        DateTime OldLastUpdated;
        string OldBody;

        public async Task<IEnumerable<ArticleContent>> GetSiblings() => await Article.ContentItems.GetList();

        public bool IsEmpty() => Body.IsEmpty() && File.IsEmpty();

        public string GetUrl() => Article.GetUrl() + "/" + Ref.ToBase32();

        public async Task<string> Render()
        {
            var highlightCss = "highlighted".OnlyWhen(Context.Current.Request().Param("faq") == ID.ToString());

            var stringBuilder = new StringBuilder();

            stringBuilder.AddFormattedLine("<div class='{0}'>", new[] { "clear-container", highlightCss, Type.Name.ToLower() }.Trim().ToString(" "));

            stringBuilder.AddFormattedLine("<div class='subject' data-ac-id=\"{5}\"><a data-toggle=\"collapse\" href=\"#collapse-{4}\" aria-controls=\"collapse-{4}\" class=\"font-weight-bold\" >{3}</a>{0}{1}</div>", await RenderEditLinks(), await RenderUpdatePanel(), GetUrl(), RenderSubjectText(), base.ID.Shorten(), base.ID);

            stringBuilder.AddFormattedLine("<div class='collapse' id='collapse-{4}'>{0}{1}{2}{3}</div>", RenderBody(), RenderCollapsedInfo(), RenderFile(), await RenderLinks(), ID.Shorten());

            stringBuilder.AppendLine("</div>");

            return stringBuilder.ToString();
        }

        public async Task<bool> IsUpdatedFor(Person user)
        {
            if (Body.IsEmpty()) return false;

            if (Body.Length < MINIMUM_CONTENT_TO_COUNT_WRITTEN) return false;

            if (user.IsInRole("Director") && UpdatedBy.IsInRole("Director")) return false;

            var study = await ArticleContentStudy.FindByContentAndUser(this, user);

            if (study == null) return true;

            return study.Updated < LastUpdated;
        }

        static string ConvertUrlsToLinks(string text)
        {
            var regexPattern = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[&#95;.a-z0-9-]+\.[a-z0-9\/&#95;:@=.+?,##%&~\-_]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
            var regex = new Regex(regexPattern, RegexOptions.IgnoreCase);
            return regex.Replace(text, "<a href=\"$1\" target=\"&#95;blank\">$1</a>").Replace("href=\"www", "href=\"http://www");
        }

        static string ConvertToBold(string line)
        {
            var startBold = "╚";
            var endBold = "╔";

            var starIndices = line.AllIndicesOf('*').ToArray();

            if (starIndices.Any() && starIndices.Count() % 2 == 0)
            {
                for (var i = 0; i < starIndices.Length; i++)
                {
                    var index = starIndices[i];

                    line = line.Remove(index, 1).Insert(index, i % 2 == 0 ? startBold : endBold);
                }
            }

            return line.Replace(startBold, "<b>").Replace(endBold, "</b>");
        }

        string RenderFile()
        {
            if (File.IsEmpty()) return null;

            var extension = File.FileExtension.ToStringOrEmpty().ToLower().TrimStart(".");

            if (extension.IsAnyOf("jpg", "jpeg", "png", "gif", "tiff", "bmp"))
                return "<img src='{0}' />".FormatWith(File.Url());

            else if (extension.IsAnyOf("mp4"))
                return "<a class='video-player' data-video-source='{0}' href='#'><img src='/images/video.jpg' /></a><div style='clear:both'></div>".FormatWith(File.Url());

            else
            {
                return "<button onclick=\"OpenBrowserWindow('{0}');\">Download</button>".FormatWith(File.Url());
            }
        }

        string RenderSubjectText()
        {
            var result = string.Empty;

            if (Type.IsAnyOf(ContentType.Custom, ContentType.FAQ, ContentType.Tip))
                result = Subject;

            result = $"{Type.Name} : {Subject}";

            return $"<span class=\"float-left mr-1 sort\"><i class=\"fas fa-arrows-alt\"></i></span>{result}";
        }

        public string RenderBody()
        {
            if (IsEmpty()) return "<red>TO BE DEFINED</red>";

            if (Type.IsHtml) return Body;

            return Body.ToLines().Trim().Select(x => x.HtmlEncode()).Select(ConvertUrlsToLinks).Select(ConvertToBold)
                .Select(ConvertLineBreaks)
                .Select(x => "<p>" + x.Replace("  ", " &nbsp;") + "</p>").ToLinesString();
        }

        string ConvertLineBreaks(string line)
        {
            if (line.StartsWith("-----")) return "<hr/>";
            else return line;
        }

        async Task<string> RenderLinks()
        {
            if (await Links.None()) return null;

            var r = new StringBuilder();
            r.AppendLine("<div class='links'>");
            r.AppendLine("<h3>Related links</h3>");

            IUrlHelper url = new UrlHelper(Context.Current.ActionContext());

            foreach (var link in await Links.GetList())
            {
                var urlPath = url.Action("Index", "Learn", new { article = ArticleId });

                r.AddFormattedLine("<a href='{0}' data-redirect=\"ajax\">{1}</a><br/>", urlPath, link.Content.FullName.HtmlEncode());
            }

            r.AppendLine("</div>");

            return r.ToString();
        }

        async Task<string> RenderEditLinks()
        {
            if (!await (await Context.Current.Person()).CanEdit(Article.Category)) return null;

            var linkBuilder = new StringBuilder();

            IUrlHelper url = new UrlHelper(Context.Current.ActionContext());

            var urlPath = url.Index("LearnContent", new { item = ID, article = ArticleId, ReturnUrl = url.Current() });

            linkBuilder.AddFormattedLine("<a name=\"Edit\" class=\"btn btn-primary btn-sm float-right\" href='{0}' data-redirect=\"ajax\">Edit</a>", urlPath);

            return linkBuilder.ToString();
        }

        string RenderCollapsedInfo()
        {
            if (CollapsedInformation.IsEmpty()) return null;

            var r = new StringBuilder();

            r.AddFormattedLine("<a class='col{0}' href='javascript:$(\".body{0}\").css(\"display\", \"block\"); $(\".col{0}\").hide();'>Show answer</a><br/>", ID);

            r.AddFormattedLine("<div style='display:none; background:#eee; padding:20px;' class='body{0}'>{1} <div class='clear-fix'> </div></div>", ID, CollapsedInformation);

            return r.ToString();
        }

        async Task<string> RenderUpdatePanel()
        {
            if (!await IsUpdatedFor(await Context.Current.Person())) return null;

            var r = new StringBuilder();

            r.Append("<div class='update-panel'>");

            if (UpdatedById.HasValue)
                r.AppendFormat("<img src='https://people.app.geeks.ltd/api/{0}/image' />", UpdatedBy.Email);

            r.Append("Last updated: " + LastUpdated.ToString("dd MMM yyyy"));

            r.Append("<a onclick=\"$.get('/@Services/Study.ashx?Content=" + ID + "'); $(this).parent().hide(); return false;\" href='#'>Confirm Read</a>");

            r.Append("</div>");

            return r.ToString();
        }

        protected override Task OnLoaded()
        {
            OldBody = Body;
            OldLastUpdated = LastUpdated;
            return base.OnLoaded();
        }

        public override async Task Validate()
        {
            await base.Validate();

            var contents = Body + Subject + CollapsedInformation;

            if (contents.Contains("<script"))
            {
                // await EmailApi.Fresh().Send(new Email { Subject = "Hack attempt", Body = UpdatedBy + " tried to submit the code: " + contents });
                throw new ValidationException("Error in the application.");
            }
        }

        protected override async Task OnValidating(EventArgs e)
        {
            LastUpdated = LocalTime.Now;

            if (!IsNew && OldBody == Body) LastUpdated = OldLastUpdated;

            OldLastUpdated = LastUpdated;
            OldBody = Body;

            await base.OnValidating(e);
        }

        protected override Task OnSaved(SaveEventArgs e)
        {
            base.OnSaved(e);

            if (Subject.IsEmpty() && Body.IsEmpty() && File.IsEmpty())
                Database.Delete(this);

            return ArticleContentStudy.ConfirmRead(UpdatedBy, this);
        }
    }
}